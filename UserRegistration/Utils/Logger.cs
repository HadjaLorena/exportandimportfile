using System.IO;

namespace UserRegistration.Utils;

public class Logger
{
    public async static void Error(string error)
    {
        string path = "logs/log-error.log";
        using StreamReader streamReader = new StreamReader(path);
        string content = await streamReader.ReadToEndAsync();

        using StreamWriter streamWriter = new StreamWriter(path);
        streamWriter.WriteLine(content + "ERROR: " + error);
    }
}
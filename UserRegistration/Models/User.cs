using System.Text;

namespace UserRegistration.Models;

public class User 
{
    public string Email {set; get;}
    public string Surname {set; get;}
    public string Name {set; get;}


    public User(string name, string surname, string email)
    {
        Name = name;
        Surname = surname;
        Email = email;
    }

    public string GetToString()
    {
        StringBuilder text = new();

        text.Append($"Email: {Email}");
        text.Append('\n');
        text.Append($"Surname: {Surname}");

        return text.ToString();
    }

    public override string ToString()
    {
        return $"{Name},{Surname},{Email}";
    }
}
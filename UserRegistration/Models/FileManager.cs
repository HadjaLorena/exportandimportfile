using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Platform.Storage;

namespace UserRegistration.Models;

public class FileManager(TopLevel topLevel)
{
    private TopLevel _topLevel = topLevel;

    public async Task ExportFile(List<User> content)
    {
        var options = new FilePickerSaveOptions
        {
            Title = "saveatfile"
        };
        var exportFile = await _topLevel.StorageProvider.SaveFilePickerAsync(options);

        if (exportFile is not null)
        {
            await using var stream = await exportFile.OpenWriteAsync();
            using var streamWrite = new StreamWriter(stream);

            foreach (var c in content)
            {
               await streamWrite.WriteLineAsync(c.ToString()); 
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Logging;
using Avalonia.Platform.Storage;
using UserRegistration.DAO;
using UserRegistration.Helpers.Observable;
using UserRegistration.Models;

namespace UserRegistration.ViewModels;

public class MainWindowViewModel(TopLevel topLevel) : ViewModelBase
{
    public static ObservableCollection<User> Users { get; set;} = [];
    private Database _database = new Database();

    public async Task ExportFile()
    {
        try {
            var options = new FilePickerSaveOptions()
            {
                Title = "Selecionar pasta",
            };
            var exportFile = await topLevel.StorageProvider.SaveFilePickerAsync(options);
            List<User> usersToExport = _database.GetUserList();

            await using var stream = await exportFile.OpenWriteAsync();
            using var streamWriter = new StreamWriter(stream);

            var json = JsonSerializer.Serialize(usersToExport);
            streamWriter.Write(json);
        } catch  {
            Utils.Logger.Error("erro ao exportar o arquivo");
        }
    }

    public async Task ImportFile()
    {
        try {
            var options = new FilePickerOpenOptions
            {
                FileTypeFilter = [FilePickerFileTypes.All],
                Title = "Selecionar arquivo JSON",
                AllowMultiple = false
            };

            var file = await topLevel.StorageProvider.OpenFilePickerAsync(options);
            await using var stream = await file[0].OpenReadAsync();
            using var reader = new StreamReader(stream);
            var content = await reader.ReadToEndAsync();

            List<User>? users = JsonSerializer.Deserialize<List<User>>(content) ?? throw new Exception();
            List<User> databaseUsers = _database.GetUserList();
            
            foreach (User u in users)
            {
                User? existsUser = databaseUsers.Find(i => i.Email == u.Email);
                
                if (existsUser == null)
                {
                    _database.SaveUser(u); 
                    Users.Add(u);
                }
            }
            
        } catch { 
            Utils.Logger.Error("erro ao importar o arquivo");
        }
    }
    
    public void InitApp()
    {
       ConnectToDatabase();
       IsConnected();
    }

    private void ConnectToDatabase()
    {
        _database = Database.CreateInstance();
    }

    private void IsConnected()
    {
        string ErrorMessage = "";
        
        if (_database == null)
        {
            ErrorMessage = "Não foi possível estabelecer a conexão com o banco de dados!";
            ErrorStack.PushError(ErrorMessage);
            throw new CustomizedErrors.DataBaseConnectionError(ErrorMessage);
        }
        Console.WriteLine("Conexão com o banco de dados feita com sucesso!");
    }
    
    public void AddNewUser(User user)
    {
        _database.SaveUser(user);
        Users.Add(user);
        
        MessageObserver messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify($"Usuário {user.Email} adicionado com sucesso!");

    }
    
    public void FilterUser(string textToFilter) {
        string textWithoutSpace = textToFilter.Trim();
        if (IsNullOrEmptyText(textWithoutSpace)) return;
        CheckIfNotHaveUserToFilter();
        
        MessageObserver messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify("Filtrando");

        List<User> filteredUsers = Users.Where(obj => obj.Email.Contains(textWithoutSpace)).ToList();

        Users.Clear();
        ImplementFilteredUsers(filteredUsers);
    }

    public bool IsNullOrEmptyText(string text) {
        if (string.IsNullOrEmpty(text)) {
            Users.Clear();
            foreach (User user in _database.GetUserList()) {
                Users.Add(user);
            }

            return true;
        }

        return false;
    }

    public void CheckIfNotHaveUserToFilter() {
        if (Users.Count == 0) {
            foreach (User user in _database.GetUserList()) {
                Users.Add(user);
            }
        }
    }

    public static void ImplementFilteredUsers(List<User> filteredUsers) {
        foreach (User user in filteredUsers) {
            Users.Add(user);
        }
    }
}
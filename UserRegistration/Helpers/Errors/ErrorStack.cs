using System;
using System.Collections.Generic;

namespace UserRegistration.Models;

public class ErrorStack
{
    private static readonly Stack<ErrorInfo> stack = new();

    public static void PushError(string errorMessage)
    {
        var errorInfo = new ErrorInfo(errorMessage, DateTime.Now);
        stack.Push(errorInfo);
    }

    public static string PopError()
    {
        if (stack.Count == 0)
        {
            return GetEmptyStackMessage();
        }
        else
        {
            var errorInfo = stack.Pop();
            return $"[{errorInfo.Timestamp}] ERRO: {errorInfo.ErrorMessage}";
        }
    }

    public static string PeekErrors()
    {
        if (stack.Count == 0) return GetEmptyStackMessage();

        string errors = "Erros na pilha:\n";
        foreach (var errorInfo in stack)
        {
            errors += $"[{errorInfo.Timestamp}] {errorInfo.ErrorMessage}\n";
        }
        return errors;
    }

    private class ErrorInfo
    {
        public string ErrorMessage { get; }
        public DateTime Timestamp { get; }

        public ErrorInfo(string errorMessage, DateTime timestamp)
        {
            ErrorMessage = errorMessage;
            Timestamp = timestamp;
        }
    }

    private static string GetEmptyStackMessage()
    {
        return "Pilha de erros vazia";
    }
}
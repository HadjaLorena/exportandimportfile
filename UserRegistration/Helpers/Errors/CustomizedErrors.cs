using System;

namespace UserRegistration.Models;

public class CustomizedErrors
{
    public class InvalidParameterError : Exception
    {
        public InvalidParameterError(string message) 
            : base($" '{message}'.")
        {
            Message = message;
        }

        public string Message { get; }
    }
    
    public class DataBaseConnectionError : Exception
    {
        public DataBaseConnectionError(string message) 
            : base($" '{message}'.")
        {
            Message = message;
        }

        public string Message { get; }
    }
}
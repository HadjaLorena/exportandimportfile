namespace UserRegistration.Helpers.Observable;

public interface IObservable
{ 
    void Update(string message);
}